//babel src/playground/es6-arrow-functions.js --out-file=public/scripts/app.js --presets=env,react --watch

// function square(x) {
//     return x*x;
// };

// // const squareArrow = (x) => {
// //     return x*x;
// // };

// const squareArrow = x => x*x;

// console.log(squareArrow(9));

const getFirstName = fullName => fullName.split(' ')[0];

console.log(getFirstName('Billy Bob'));
//babel src/playground/es6-arrow-functions-2.js --out-file=public/scripts/app.js --presets=env,react --watch

//arguments object - no loner bound with arrow functions

const add = (a,b) => {
    // console.log(arguments)
    return a+b;
};

console.log(add(55,1,7))




//this keyword - no longer bound
const user = {
    name:'Billy',
    cities:['Philadelphia','New York','Dublin'],
    printPlacesLived () {
        return this.cities.map((city) => this.name + ' has lived in ' + city);
    }
};

console.log(user.printPlacesLived());

const multiplyer = {
    numbers: [22, 5, 8],
    multiplyBy: 2,
    multiply() {
        return this.numbers.map(num => num * this.multiplyBy);
    }
}

console.log(multiplyer.multiply());
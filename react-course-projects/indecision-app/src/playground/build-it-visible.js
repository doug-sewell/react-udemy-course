let visible = false;

const onVisibilityToggle = () => {
    visible = !visible;
    render();
}

const render = () => {
    const app = (
        <div>
            <h1>Visibility Toggle</h1>
            <button onClick={onVisibilityToggle}>
            {visible ? 'Hide details' : 'Show details'}
            </button>
            {visible && <p>Hey. These are some details you can now see!</p>}
        </div>
    );
    ReactDOM.render(app, document.getElementById('app'));
};

render();

//babel src/playground/build-it-visible.js --out-file=public/scripts/app.js --presets=env,react --watch